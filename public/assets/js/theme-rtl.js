// Toggle RTL mode for demo
// ----------------------------------- 


(function(window, document, $, undefined){

  $(function(){
    var maincss = $('#maincss');
    var bscss = $('#bscss');
    $('#chk-rtl').on('change', function(){
      
      // app rtl check
      maincss.attr('href', this.checked ? '/assets/css/theme-rtl.css' : '/assets/css/theme.css' );
      // bootstrap rtl check
      bscss.attr('href', this.checked ? '/assets/css/bootstrap-rtl.css' : '/assets/css/bootstrap.css' );

    });

  });

})(window, document, window.jQuery);
