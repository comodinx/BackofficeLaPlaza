
(function(window, document, $, undefined){

  $(function(){

        toastr.options = {
            debug: false,
            newestOnTop: false,
            positionClass: 'toast-top-center',
            closeButton: true,
            preventDuplicates: true,
            toastClass: 'animated fadeInDown',
        };

  });

})(window, document, window.jQuery);
