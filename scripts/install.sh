#!/bin/bash


### Constants variables
BASE_DIR_NAME=$(printf '%s\n' "${PWD##*/}")
REPO=${PWD};

if [ "${PWD##*/}" != "scripts" ]
then
    cd "scripts";
else
    REPO='${PWD}/..'
fi

SCRIPTS=${PWD};


### Local variables
HELP=false
PROYECT_NAME=''
PROYECT_VERSION=''
EXTBKP=''


### Source utilities
source "${SCRIPTS}/helpers.sh"


### Parse arguments
while getopts ":n:v:h" opt; do
    case "${opt}" in
        n)  PROYECT_NAME=$OPTARG;;
        v)  PROYECT_VERSION=$OPTARG;;
        h)  HELP=true;;
    esac
done


### Check arguments
if [ "$HELP" != false ]
then
    log_help "./scripts/install.sh -n \"MyProyectName\" -v \"1.0.0\""
    exit 0
fi

cd $REPO;
if [ -z "${PROYECT_NAME}" ]
then
    PROYECT_NAME="${BASE_DIR_NAME}"
    log_warn "Not Proyect name specified, the default name is $PROYECT_NAME. For set name use \"-n\" argument"
fi

BASE_APP_NAME=$(app_name)
if [ "${BASE_APP_NAME}" == "${PROYECT_NAME}" ]
then
    log_warn "Proyect $PROYECT_NAME is already install"
    exit 1
fi

if [ -z "${PROYECT_VERSION}" ]
then
    PROYECT_VERSION=$(app_version)
    log_warn "Not Proyect version specified, the default version is $PROYECT_VERSION. For set version use \"-v\" argument"
fi

if [[ "$OSTYPE" == "darwin"* ]]
then
    EXTBKP='.bkp'
fi


### Source function
log_title "Preparing for instalation $PROYECT_NAME v$PROYECT_VERSION"

# Name
log_step "Change proyect name in necesary files..."
sudo sed -i $EXTBKP "s/backend-base/$PROYECT_NAME/g" "$REPO/package.json"
sudo sed -i $EXTBKP "s/backend-base/$PROYECT_NAME/g" "$REPO/README.md"

# Version
log_step "Change proyect version..."
app_version "$PROYECT_VERSION"

# Clean backups
log_step "Clean directories..."

rm -rf .git/

if [ -f "package.json$EXTBKP" ]
then 
    rm -f "package.json$EXTBKP"
fi

if [ -f "README.md$EXTBKP" ]
then 
    rm -f "README.md$EXTBKP"
fi

./scripts/reinstall.sh

log_ok "Install OK"
