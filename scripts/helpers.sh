#!/bin/bash
#


# Globals
# ================================

APP_NAME="Backoffice La Plaza"


# Helpers
# ================================

parse_json()
{
    echo $1 | \
    sed -e 's/[{}]/''/g' | \
    sed -e 's/", "/'\",\"'/g' | \
    sed -e 's/" ,"/'\",\"'/g' | \
    sed -e 's/" , "/'\",\"'/g' | \
    sed -e 's/","/'\"---SEPERATOR---\"'/g' | \
    awk -F=':' -v RS='---SEPERATOR---' "\$1~/\"$2\"/ {print}" | \
    sed -e "s/\"$2\"://" | \
    tr -d "\n\t" | \
    sed -e 's/\\"/"/g' | \
    sed -e 's/\\\\/\\/g' | \
    sed -e 's/^[ \t]*//g' | \
    sed -e 's/^"//'  -e 's/"$//'
}

check_command()
{
    local commands=$(command -v $1 | grep $1 | wc -l)
    # Default is unavailable
    local isAvailable=1

    if [ $commands -gt 0 ]
    then
        isAvailable=0
    fi

    return $isAvailable
}


# Application
# ================================

app_name()
{
    local currentPath="${PWD}"

    if [ "${PWD##*/}" == "scripts" ]
    then
        cd "..";
    fi
    echo $(node -p "require('./package.json').name")
    cd "${currentPath}"
}

app_version()
{
    local version="$1"
    local changed="$(npm version ${version} 2> /dev/null)"
    local hasError="^npm ERR"
    local currentPath="${PWD}"
    local hasVersionNotChange="Version not changed"

    if [ "${PWD##*/}" == "scripts" ]
    then
        cd "..";
    fi

    if [ -z "${version}" ]
    then
        echo $(node -p "require('./package.json').version")
        cd "${currentPath}"
        return 0
    fi

    if [[ ! ${changed} =~ ${hasError} ]]
    then
        if [[ ${changed} =~ ${hasVersionNotChange} ]]
        then
            cd "${currentPath}"
            return 1
        fi
    fi
}


# Logger
# ================================

log()
{
    echo -e [$(date +"%Y-%m-%d %H:%M:%S")]" $1"
}

log_help()
{
    echo -e "\033[1;37m$(tput sgr 0 1)$(tput bold)Usage$(tput sgr0):\033[0m $1"
}

log_title()
{
    local title=$1
    local underline=''
    local length=${#title}

    echo -e "\n${title}"

    local i=1
    until [  $i -gt $length ]; do
        underline="${underline}-"
        let i+=1
    done
    echo -e "\033[0;32m${underline}\033[0m\n"
}

log_step()
{
    echo -e "\033[0;32m•\033[0m $1"
}

log_ok()
{
    echo -e "\033[0;32m✔\033[0m $1"
}

log_debug()
{
    echo -e "$1\033[0m"
}

log_warn()
{
    echo -e "\033[0;33m➜ $1\033[0m"
}

log_error()
{
    echo -e "\033[0;31m✘ Oooops...\033[0m $1"
}
