#!/bin/bash


### Constants variables
REPO=${PWD};

if [ "${PWD##*/}" != "scripts" ]
then
    cd "scripts";
else
    REPO='${PWD}/..'
fi

SCRIPTS=${PWD};


### Local variables
HELP=false
OVERRIDE_VERSION=false


### Source utilities
source "${SCRIPTS}/helpers.sh"


### Parse arguments
while getopts ":s:b:l:r:v:oh" opt; do
    case "${opt}" in
        s) STAGE=${OPTARG};;
        b) BRANCH=${OPTARG};;
        l) LEVEL=${OPTARG};;
        r) REMOTE=${OPTARG};;
        v) VERSION=${OPTARG};;
        o) OVERRIDE_VERSION=true;;
        h) HELP=true;;
    esac
done


### Check arguments
if [ "$HELP" != false ]
then
    log_help "./scripts/deploy.sh -v \"1.0.0\""
    exit 0
fi

if [ -z "${STAGE}" ]
then
    STAGE="testing"
fi

if [ "${STAGE}" != "testing" ] && [ "${STAGE}" != "production" ]
then
    log_warn "Fatal Error: Invalid Stage"
    exit 1
fi

if [ -z "${BRANCH}" ]
then
    BRANCH="master"
fi

if [ -z "${LEVEL}" ]
then
    LEVEL="patch"
fi

if [ "${LEVEL}" != "major" ] && [ "${LEVEL}" != "minor" ] && [ "${LEVEL}" != "patch" ]
then
    log_warn "Fatal Error: Invalid Level"
    exit 1
fi

if [ -z "${REMOTE}" ]
then
    REMOTE="origin"
fi

JOB=$(app_name)


### Source functionality
log_title "DEPLOYING $APP_NAME"

cd $REPO;

versioned() {
    if [ "$OVERRIDE_VERSION" != false ]
    then
        VERSION=$(app_version)
        log_title "Override current version..."
    else
        log_title "Creating version..."
    fi

    git fetch -p
    git fetch -p ${REMOTE} +refs/tags/*:refs/tags/*
    git checkout ${BRANCH}
    git rebase ${REMOTE}/${BRANCH}

    if [ -n "${VERSION}" ]
    then
        app_version "${VERSION}"
    else
        app_version "${LEVEL}"
    fi

    git push ${REMOTE} ${BRANCH}
}

deploymentHeroku() {
    log_title "Deployment on heroku..."
    git push heroku master
}

case "${STAGE}" in
    testing)
        # Generate new version if necesary
        versioned;
        deploymentHeroku;
        ;;

    production)
        deploymentHeroku;
        ;;
esac


log_ok "Done!"
