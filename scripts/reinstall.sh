#!/bin/bash


### Constants variables
NODE_MODULES="node_modules";
LOGS="/var/log/application";
REPO=${PWD};

if [ "${PWD##*/}" != "scripts" ]
then
    cd "scripts";
else
    REPO='${PWD}/..'
fi

SCRIPTS=${PWD};


### Local variables
HELP=false


### Source utilities
source "${SCRIPTS}/helpers.sh"


### Parse arguments
while getopts ":h" opt; do
    case "${opt}" in
        h)  HELP=true;;
    esac
done


### Check arguments
if [ "$HELP" != false ]
then
    log_help "./scripts/reinstall.sh"
    exit 0
fi


### Source function
log_title "Reinstaling..."

cd $REPO;
if [ ! -d "$LOGS" ]
then
    log_step "Creating logs directory...\n";
    sudo mkdir $LOGS;
    sudo chown -R $USER $LOGS;
fi

log_step "Installing node modules...\n";
if [ -d "$NODE_MODULES" ]
then
    rm -f "package-lock.json";
    rm -rf $NODE_MODULES;
fi
npm install


log_ok "Reinstall OK"
