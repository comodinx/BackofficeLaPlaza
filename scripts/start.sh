#!/bin/bash


### Constants variables
REPO=${PWD};

if [ "${PWD##*/}" != "scripts" ]
then
    cd "scripts";
else
    REPO='${PWD}/..'
fi

SCRIPTS=${PWD};


### Local variables
HELP=false
PORT=''


### Source utilities
source "${SCRIPTS}/helpers.sh"


### Parse arguments
while getopts ":s:p:h" opt; do
    case "${opt}" in
        s) STAGE=${OPTARG};;
        p) PORT="PORT=${OPTARG}";;
        h) HELP=true;;
    esac
done


### Check arguments
if [ "$HELP" != false ]
then
    log_help "./scripts/start.sh -s \"local\" -p \"8080\""
    exit 0
fi

if [ -z "${STAGE}" ]
then
    STAGE='production'
fi

if [ "${STAGE}" != "local" ] && [ "${STAGE}" != "production" ]
then
    log_warn "Fatal Error: Invalid Stage"
    exit 1
fi


### Source functionality
log_title "STARTING $APP_NAME";

cd $REPO;
if [ "${STAGE}" == "local" ]
then
    NODE_ENV=development grunt start
    exit 0
fi

NODE_ENV=${STAGE} $PORT nodejs index.js > /var/log/application/${APP_ID}.log 2> /var/log/application/${APP_ID}.error.log &
