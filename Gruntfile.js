'use strict';

module.exports = grunt => {
    require('./grunt')(grunt);

    grunt.registerTask('start', ['force:eslint:grunt', 'force:eslint:server', 'concurrent:start']);
    grunt.registerTask('deploy', ['clean:dist', 'compress:dist']);
};
