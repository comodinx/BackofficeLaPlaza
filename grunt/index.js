'use strict';

const fs = require('fs');
const path = require('path');

module.exports = grunt => {
    require('load-grunt-config')(grunt, {
        configPath: path.join(__dirname, 'contrib')
    });
    fs.readdirSync(path.join(__dirname, 'tasks')).forEach(filename => {
        let name = path.basename(filename, '.js');

        grunt.registerTask(name, require(`./tasks/${name}`)(grunt));
    });
};
