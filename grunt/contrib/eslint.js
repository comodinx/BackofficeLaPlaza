'use strict';

module.exports = grunt => {
    return {
        grunt: 'grunt/**/*.js',
        server: ['*.js', 'server/**/*.js'],
        options: {
            format: grunt.option('format') || 'stylish',
            outputFile: grunt.option('output')
        }
    };
};
