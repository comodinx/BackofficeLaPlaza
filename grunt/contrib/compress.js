'use strict';

module.exports = grunt => {
    let dest = `/${grunt.option('dist_destination') || ''}`;

    return {
        dist: {
            options: {
                mode: 'tgz',
                archive: `dist/${grunt.option('dist_target') || 'application.tar.gz'}`
            },
            files: [{
                src: ['public/**'],
                dest
            }, {
                src: ['server/**'],
                dest
            }, {
                src: ['node_modules/**'],
                dest
            }, {
                src: ['index.js', 'package.json'],
                dest
            }]
        }
    };
};
