'use strict';

module.exports = () => {
    return {
        start: {
            script: 'index.js',
            options: {
                watch: ['.changes'],
                ext: '.reboot',
                callback(nodemon) {
                    nodemon.on('log', onLog);
                }
            }
        }
    };
};

function onLog(event) {
    console.log(event.colour);
}
