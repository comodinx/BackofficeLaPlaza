'use strict';

module.exports = () => {
    return {
        options: {
            logConcurrentOutput: true
        },
        start: {
            tasks: ['nodemon:start', 'watch']
        }
    };
};
