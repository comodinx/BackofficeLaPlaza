'use strict';

module.exports = () => {
    return {
        server: {
            files: ['index.js', 'server/**/*.js'],
            tasks: ['force:eslint:server', 'reboot']
        },
        client: {
            files: ['views/**/*.html'],
            tasks: ['reboot']
        }
    };
};
