'use strict';

module.exports = grunt => {
    return () => {
        grunt.file.mkdir('.changes');
        grunt.file.write('.changes/nodemon.reboot', Date.now());
    };
};
