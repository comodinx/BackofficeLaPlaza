'use strict';

let Base = require('backend-library/database/mongo/model');

class Model extends Base {

    constructor(options) {
        super(options);
    }

}

module.exports = Model;
