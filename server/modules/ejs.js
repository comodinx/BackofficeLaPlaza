'use strict';

let _ = require('underscore');
let ejs = require('ejs');
let path = require('path');
let moment = require('backend-library/utils/moment');

class EJS {

    constructor(app, options) {
        this.app = app;
        this.options = _.extend({}, options || {});
    }

    start() {
        this.app.set('view engine', 'ejs');
        this.app.set('views', path.join(__dirname, '..', '..', 'views'));
        this.app.engine('.html', ejs.renderFile);

        this.app.locals = _.extend(this.app.locals || {}, {
            formatDate: this.formatDate,
            fromNow: this.fromNow,
            cutText: this.cutText
        });
    }

    formatDate(date, pattern = 'MMM Do YY') {
        return moment(moment.appParse(date)).format(pattern);
    }

    fromNow(date) {
        return moment(moment.appParse(date)).fromNow();
    }

    cutText(text, limit = 50, suffix = '...') { // eslint-disable-line no-magic-numbers
        if (!text || text.length <= limit) {
            return text;
        }
        return text.substring(0, limit) + suffix;
    }

}

module.exports = EJS;
