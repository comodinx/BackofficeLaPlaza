'use strict';

const low = require('lowdb');
const lodashId = require('lodash-id');
const FileSync = require('lowdb/adapters/FileSync');
const Collection = require('./collection');
const adapter = new FileSync(`${__dirname}/../../data/db.json`);
const db = low(adapter);

db._.mixin(lodashId);
db._.mixin(Collection);

module.exports = {
    db,
    models: {
        animalType: db.get('animal_type'),
        food: db.get('foods'),
        foodBrand: db.get('food_brands'),
        foodCategory: db.get('food_categories'),
        accessory: db.get('accessories')
    }
};
