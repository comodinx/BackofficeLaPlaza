'use strict';

const _ = require('underscore');
const fs = require('backend-library/utils/fs');
const Multipart = require('backend-library/server/middleware/multipart');
const Promisify = require('backend-library/server/middleware/promisify');

let middlewares = {};

Multipart.id = 'multipart';
middlewares.multipart = Multipart;

Promisify.id = 'promisify';
middlewares.promisify = Promisify;

_.extend(middlewares, _.mapObject(fs.requiredirSync(__dirname), (Middleware, id) => {
    Middleware.id = id;
    return Middleware;
}));

module.exports = middlewares;
