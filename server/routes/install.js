'use strict';

const path = require('path');
const Base = require('backend-library/server/router/route');

class Route extends Base {

    get route() {
        return '/install/:environment*?';
    }

    handler(req, res) {
        res.download(path.join(__dirname, '..', `/data/android-${req.params.environment || 'dev'}.apk`));
    }

}

module.exports = Route;
