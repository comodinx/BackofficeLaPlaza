'use strict';

const Base = require('backend-library/server/router/route');
const moment = require('backend-library/utils/moment');
const Database = require('../modules/database');
const Food = Database.models.food;

class Route extends Base {

    get route() {
        return '/';
    }

    handler(req, res) {
        res.render('foods/index.html', {
            foods: Food
                .filter(food => {
                    return !food.deletedAt;
                })
                .orderBy(food => {
                    return moment.appParse(food.createdAt).getTime();
                }, 'desc')
                .value()
        });
    }

}

module.exports = Route;
