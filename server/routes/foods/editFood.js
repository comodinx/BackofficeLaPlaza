'use strict';

const Base = require('backend-library/server/router/route');
const Database = require('../../modules/database');
const FoodBrand = Database.models.foodBrand;
const FoodCategory = Database.models.foodCategory;
const Food = Database.models.food;

class Route extends Base {

    get route() {
        return '/:id/edit';
    }

    handler(req, res) {
        let food = Food.getById(Number(req.params && req.params.id || -1)).value();

        if (!food) {
            return res.render('errors/404.html');
        }

        return res.render('foods/editFood.html', {
            foodBrands: FoodBrand.value(),
            foodCategories: FoodCategory.value(),
            food
        });
    }

}

module.exports = Route;
