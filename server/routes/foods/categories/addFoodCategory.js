'use strict';

const Base = require('backend-library/server/router/route');

class Route extends Base {

    get route() {
        return '/add';
    }

    handler(req, res) {
        return res.render('foods/categories/addFoodCategory.html');
    }

}

module.exports = Route;
