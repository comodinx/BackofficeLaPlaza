'use strict';

const Base = require('backend-library/server/router/route');
const Database = require('../../../modules/database');
const FoodCategory = Database.models.foodCategory;

class Route extends Base {

    get route() {
        return '/:id/edit';
    }

    handler(req, res) {
        let foodCategory = FoodCategory.getById(Number(req.params && req.params.id || -1)).value();

        if (!foodCategory) {
            return res.render('errors/404.html');
        }

        return res.render('foods/categories/editFoodCategory.html', {
            foodCategory
        });
    }

}

module.exports = Route;
