'use strict';

const P = require('backend-library/utils/p');
const Base = require('backend-library/server/router/route');
const http = require('backend-library/utils/http');
const moment = require('backend-library/utils/moment');
const Database = require('../../../modules/database');
const FoodCategory = Database.models.foodCategory;

class Route extends Base {

    get route() {
        return '/:id';
    }

    get method() {
        return http.method.DELETE;
    }

    handle(req) {
        return new P(resolve => {
            let foodCategory = FoodCategory.getById(Number(req.params && req.params.id || -1)).value();

            if (!foodCategory) {
                resolve({
                    status: http.status.NOT_FOUND,
                    body: {
                        code: http.status.NOT_FOUND,
                        description: 'failure'
                    }
                });
                return;
            }

            let data = {
                deletedAt: moment.appFormat(new Date())
            };

            FoodCategory.updateById(req.params.id, data).write();

            resolve({
                status: http.status.NO_CONTENT,
                body: {
                    code: http.status.NO_CONTENT,
                    description: 'success'
                }
            });
        });
    }

}

module.exports = Route;
