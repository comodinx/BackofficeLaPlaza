'use strict';

const Base = require('backend-library/server/router/route');
const Database = require('../../../modules/database');
const FoodCategory = Database.models.foodCategory;

class Route extends Base {

    handler(req, res) {
        res.render('foods/categories/index.html', {
            foodCategories: FoodCategory
                .filter(foodCategory => {
                    return !foodCategory.deletedAt;
                })
                .value()
        });
    }

}

module.exports = Route;
