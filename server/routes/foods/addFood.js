'use strict';

const Base = require('backend-library/server/router/route');
const Database = require('../../modules/database');
const FoodBrand = Database.models.foodBrand;
const FoodCategory = Database.models.foodCategory;

class Route extends Base {

    get route() {
        return '/add';
    }

    handler(req, res) {
        return res.render('foods/addFood.html', {
            foodBrands: FoodBrand.value(),
            foodCategories: FoodCategory.value()
        });
    }

}

module.exports = Route;
