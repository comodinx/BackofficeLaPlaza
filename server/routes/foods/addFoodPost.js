'use strict';

const P = require('backend-library/utils/p');
const Base = require('backend-library/server/router/route');
const http = require('backend-library/utils/http');
const moment = require('backend-library/utils/moment');
const Database = require('../../modules/database');
const Food = Database.models.food;

class Route extends Base {

    get method() {
        return http.method.POST;
    }

    handle(req) {
        return new P(resolve => {
            let id = Food.size().value() + 1;

            let data = {
                id,
                title: req.body.title,
                detailDescription: req.body.detailDescription,
                tecnicalDescription: req.body.tecnicalDescription,
                idFoodBrand: Number(req.body.idFoodBrand),
                idFoodCategory: Number(req.body.idFoodCategory),
                weight: Number(req.body.weight),
                stock: Number(req.body.stock),
                price: {
                    pouch: Number(req.body.pricePouch),
                    kg: Number(req.body.priceKG)
                },
                createdAt: moment.appFormat(new Date())
            };

            if (req.files && req.files.image && req.files.image.value) {
                data.image = req.files.image.value.toString('base64');
            }

            Food.insert(data).write();

            resolve({
                status: http.status.OK,
                body: {
                    code: http.status.OK,
                    description: 'success'
                }
            });
        });
    }

}

module.exports = Route;
