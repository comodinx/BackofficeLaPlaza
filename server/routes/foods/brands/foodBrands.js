'use strict';

const Base = require('backend-library/server/router/route');
const Database = require('../../../modules/database');
const FoodBrand = Database.models.foodBrand;

class Route extends Base {

    handler(req, res) {
        res.render('foods/brands/index.html', {
            foodBrands: FoodBrand
                .filter(foodBrand => {
                    return !foodBrand.deletedAt;
                })
                .value()
        });
    }

}

module.exports = Route;
