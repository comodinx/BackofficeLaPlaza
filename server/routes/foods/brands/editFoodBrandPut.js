'use strict';

const P = require('backend-library/utils/p');
const Base = require('backend-library/server/router/route');
const http = require('backend-library/utils/http');
const moment = require('backend-library/utils/moment');
const Database = require('../../../modules/database');
const FoodBrand = Database.models.foodBrand;

class Route extends Base {

    get route() {
        return '/:id';
    }

    get method() {
        return http.method.PUT;
    }

    handle(req) {
        return new P(resolve => {
            let foodBrand = FoodBrand.getById(Number(req.params && req.params.id || -1)).value();

            if (!foodBrand) {
                resolve({
                    status: http.status.NOT_FOUND,
                    body: {
                        code: http.status.NOT_FOUND,
                        description: 'failure'
                    }
                });
                return;
            }

            let data = {
                description: req.body.description,
                updatedAt: moment.appFormat(new Date())
            };

            FoodBrand.updateById(req.params.id, data).write();

            resolve({
                status: http.status.OK,
                body: {
                    code: http.status.OK,
                    description: 'success'
                }
            });
        });
    }

}

module.exports = Route;
