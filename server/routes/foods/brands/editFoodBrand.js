'use strict';

const Base = require('backend-library/server/router/route');
const Database = require('../../../modules/database');
const FoodBrand = Database.models.foodBrand;

class Route extends Base {

    get route() {
        return '/:id/edit';
    }

    handler(req, res) {
        let foodBrand = FoodBrand.getById(Number(req.params && req.params.id || -1)).value();

        if (!foodBrand) {
            return res.render('errors/404.html');
        }

        return res.render('foods/brands/editFoodBrand.html', {
            foodBrand
        });
    }

}

module.exports = Route;
