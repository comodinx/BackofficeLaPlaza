'use strict';

const P = require('backend-library/utils/p');
const Base = require('backend-library/server/router/route');
const http = require('backend-library/utils/http');
const moment = require('backend-library/utils/moment');
const Database = require('../../../modules/database');
const FoodBrand = Database.models.foodBrand;

class Route extends Base {

    get method() {
        return http.method.POST;
    }

    handle(req) {
        return new P(resolve => {
            let id = FoodBrand.size().value() + 1;

            let data = {
                id,
                description: req.body.description,
                createdAt: moment.appFormat(new Date())
            };

            FoodBrand.insert(data).write();

            resolve({
                status: http.status.OK,
                body: {
                    code: http.status.OK,
                    description: 'success'
                }
            });
        });
    }

}

module.exports = Route;
