'use strict';

const Base = require('backend-library/server/router/route');

class Route extends Base {

    get route() {
        return '/add';
    }

    handler(req, res) {
        return res.render('foods/brands/addFoodBrand.html');
    }

}

module.exports = Route;
