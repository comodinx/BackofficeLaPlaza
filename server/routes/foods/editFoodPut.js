'use strict';

const P = require('backend-library/utils/p');
const Base = require('backend-library/server/router/route');
const http = require('backend-library/utils/http');
const moment = require('backend-library/utils/moment');
const Database = require('../../modules/database');
const Food = Database.models.food;

class Route extends Base {

    get route() {
        return '/:id';
    }

    get method() {
        return http.method.PUT;
    }

    handle(req) {
        return new P(resolve => {
            let food = Food.getById(Number(req.params && req.params.id || -1)).value();

            if (!food) {
                resolve({
                    status: http.status.NOT_FOUND,
                    body: {
                        code: http.status.NOT_FOUND,
                        description: 'failure'
                    }
                });
                return;
            }

            let data = {
                title: req.body.title,
                detailDescription: req.body.detailDescription,
                tecnicalDescription: req.body.tecnicalDescription,
                idFoodBrand: Number(req.body.idFoodBrand),
                idFoodCategory: Number(req.body.idFoodCategory),
                weight: Number(req.body.weight),
                stock: Number(req.body.stock),
                price: {
                    pouch: Number(req.body.pricePouch),
                    kg: Number(req.body.priceKG)
                },
                updatedAt: moment.appFormat(new Date())
            };

            if (req.files && req.files.image && req.files.image.value) {
                data.image = req.files.image.value.toString('base64');
            }

            Food.updateById(req.params.id, data).write();

            resolve({
                status: http.status.OK,
                body: {
                    code: http.status.OK,
                    description: 'success'
                }
            });
        });
    }

}

module.exports = Route;
