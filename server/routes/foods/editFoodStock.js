'use strict';

const _ = require('underscore');
const e = require('backend-library/utils/e');
const P = require('backend-library/utils/p');
const Base = require('backend-library/server/router/route');
const http = require('backend-library/utils/http');
const Database = require('../../modules/database');
const validator = require('validator');
const Food = Database.models.food;

class Route extends Base {

    get route() {
        return '/:id/stock/:action';
    }

    get method() {
        return http.method.PUT;
    }

    validate(req) {
        super.validate(req);

        if (_.isEmpty(req.params)) {
            throw new e.ValidationException('Necesita parametros "id" and "acción"');
        }
        else if (_.isEmpty(req.params.id)) {
            throw new e.ValidationException('Necesita parametro "id"');
        }
        else if (!validator.isInt(req.params.id)) {
            throw new e.ValidationException('Necesita parametro "id" númerico');
        }
        else if (_.isEmpty(req.params.action)) {
            throw new e.ValidationException('Necesita parametro "acción"');
        }
        else if (req.params.action !== 'increase' && req.params.action !== 'decrease') {
            throw new e.ValidationException('Necesita parametro "acción" con valor "increase" o "decrease"');
        }
    }

    handle(req) {
        return new P(resolve => {
            let food = Food.getById(Number(req.params && req.params.id || -1)).value();

            if (!food) {
                resolve({
                    status: http.status.NOT_FOUND,
                    body: {
                        code: http.status.NOT_FOUND,
                        description: 'failure'
                    }
                });
                return;
            }

            let data = {
                stock: Number(food.stock)
            };

            switch (req.params.action) {
            case 'increase':
                data.stock++;
                break;

            case 'decrease':
                data.stock--;
                break;
            }

            Food.updateById(req.params.id, data).write();

            resolve({
                status: http.status.OK,
                body: {
                    code: http.status.OK,
                    description: 'success'
                }
            });
        });
    }

}

module.exports = Route;
