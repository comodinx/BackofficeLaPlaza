'use strict';

const Base = require('backend-library/server/router/route');

class Route extends Base {

    get route() {
        return '/termsAndConditions';
    }

    handler(req, res) {
        res.render('others/termsAndConditions.html');
    }

}

module.exports = Route;
