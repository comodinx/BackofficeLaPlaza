'use strict';

const Base = require('backend-library/server/router/route');

class Route extends Base {

    get route() {
        return '/privacyPolicies';
    }

    handler(req, res) {
        res.render('others/privacyPolicies.html');
    }

}

module.exports = Route;
