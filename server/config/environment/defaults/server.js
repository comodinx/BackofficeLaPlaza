'use strict';

const PORT = process.env.PORT || 5000;
const IP = process.env.IP || '0.0.0.0';

module.exports = {
    port: PORT,
    ip: IP
};
