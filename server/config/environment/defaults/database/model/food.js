'use strict';

const Schema = require('mongoose').Schema;

module.exports = {
    name: 'Food',
    options: new Schema({
        title: String,
        detailDescription: String,
        tecnicalDescription: String,
        image: String,
        createdAt: Date
    })
};
