'use strict';

const NAME = require('../../../../package.json').name;

module.exports = {
    mock: true,
    prefix: `application.${NAME}`,
    global_tags: [NAME] // eslint-disable-line camelcase
};
