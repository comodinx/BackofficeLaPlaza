'use strict';

const Config = require('backend-library/config');

// Shared instance for all application context
Config.shared = new Config({
    configFile: `${__dirname}/environment`,
    'package': require('../../package.json')
});

// Export instance
module.exports = Config.shared;
