'use strict';

// Need read config in this line, for create shared instance.
const Config = require('./config');

require('backend-library/dependencies/uncaught');

const _ = require('underscore');
const path = require('path');
const express = require('express');
const favicon = require('serve-favicon');
const EJS = require('./modules/ejs');
const Morgan = require('backend-library/modules/morgan');
const Winston = require('backend-library/modules/winston');
const Base = require('backend-library/server');
const Core = require('backend-library/server/core');
const middlewares = require('./middlewares');

const IS_LOG_ENABLED = Config.get('server:log:enabled', true);
const IS_LOG_DISABLED = !!process.env.IS_SERVER_LOG_DISABLED;

class Server extends Base {

    constructor() {
        super({
            isLogEnabled: IS_LOG_ENABLED && !IS_LOG_DISABLED,
            middlewares
        });
    }

    configure() {
        this.modules();

        super.configure();

        new EJS(this.app).start();

        this.app.use(express.static('public'));
        this.app.use(favicon(path.join(__dirname, '..', 'public', 'favicon.ico')));
    }

    bootstrap() {
        super.bootstrap();

        this.core = new Core({
            dirname: `${__dirname}/routes`,
            app: this.app
        });

        return this.core.start();
    }

    modules() {
        _.each([Winston, Morgan], module => {
            module.sharize({
                app: this.app
            });
        });
    }

}

module.exports = Server;
