# backoffice-laplaza

### Downloading and installing
```sh
$ git clone git@gitlab.com:comodinx/backoffice-laplaza.git <YOUR PROYECT NAME>
$ cd <YOUR PROYECT NAME>
$ ./scripts/install.sh
```

### Running
To use another port or host/ip, you must set the PORT and/or IP environment variable.

##### Development
This way the Node.js environment automatically restarts when it detects changes in the source code.
```sh
$ npm run start:development
# OR
$ npm run start:d
```

##### Production
```sh
$ npm start
# OR
$ node index
```

### Deploy
```sh
$ npm run deploy
# Go to jenkins and run
```
